package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestCalculator {

    @Test
    public void testGetFahrenheit() throws Exception{
        Calculator calculator = new Calculator();
        double price = calculator.getFahrenheit("50");
        Assertions.assertEquals(10.0, price, 0.0, "Price of book 1");

    }
}
